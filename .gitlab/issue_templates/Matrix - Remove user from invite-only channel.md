# Remove user from matrix invite-only channel #

## Channel ##

*Replace this line with the matrix channel to remove user from*

## User ##

*Replace this line with the matrix user to remove*

/label ~Service::Matrix
/confidential
