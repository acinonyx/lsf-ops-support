# Modify CADLAB members #

## Project name ##

*Replace this line with the CADLAB project name*

## Add members ##

  * *Replace this line with a CADLAB username*
  * *...*
  * *...*

## Remove members ##

  * *Replace this line with a CADLAB username*
  * *...*
  * *...*

/label ~Service::CADLAB
/confidential
