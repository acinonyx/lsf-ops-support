# Destroy short URL #

## Shortened URL ##

*Replace this line with the short URL that should be destroyed*
*Domains available: s.libre.space and satno.gs*

/label ~Service::YOURLS
/confidential
