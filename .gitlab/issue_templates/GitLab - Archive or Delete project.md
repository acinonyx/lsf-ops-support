# Archive or Delete GitLab project #

## Project path ##

*Replace this line with the full project path; e.g. librespacefoundation/satnogs/satnogs-network*

## Settings ##

  - [ ] Delete project

/label ~Service::GitLab
/confidential
