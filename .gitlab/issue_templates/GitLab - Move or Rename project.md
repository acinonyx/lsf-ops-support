# Move or Rename GitLab project #

## Project path ##

*Replace this line with the full project path; e.g. librespacefoundation/satnogs/satnogs-network*

## New project path ##

*Replace this line with the project new full project path; e.g. librespacefoundation/satnogs/satnogs-network-new*

/label ~Service::GitLab
/confidential
