# Disable email account #

## Email address ##

*Replace this line with the email address of the account to disable; e.g. me@libre.space*

/label ~Service::Email
/confidential
