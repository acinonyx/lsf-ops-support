# Create email account #

## Full name ##

*Replace this line with your full name*

## Invitation email address ##

*Replace this line with the email address to which the invitation will be sent; e.g. me@gmail.com*

## Email address ##

*Replace this line with the email address of the new account; e.g. me@libre.space*

## Account type ##

  * Contributor
    * [ ] Core
	* [ ] Paid
	* [ ] Board

## Aliases (optional) ##

  * *Replace this line with the email address of the alias; e.g. alias@libre.space*
  * *...*
  * *...*

/label ~Service::Email
/confidential
