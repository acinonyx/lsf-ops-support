# Modify GitLab subgroup developers #

## Group path ##

*Replace this line with the full group path; e.g. librespacefoundation/satnogs*

## Add developers ##

  * *Replace this line with a developer's GitLab username; e.g. @acinonyx*
  * *...*
  * *...*

## Remove developers ##

  * *Replace this line with a developer's GitLab username; e.g. @acinonyx*
  * *...*
  * *...*

/label ~Service::GitLab
/confidential
