# Create GitLab group #

## Group name ##

*Replace this line with the group name; e.g. SatNOGS*

## Group path ##

*Replace this line with the full group path; e.g. librespacefoundation/satnogs*

## Developers ##

  * *Replace this line with a developer's GitLab username; e.g. @acinonyx*
  * *...*
  * *...*

/label ~Service::GitLab
/confidential
