# Create ELN team #

## Team name ##

*Replace this line with the team name; e.g. MyTeam*

## Administrator ##

*Replace this line with the email address of the user to be promoted to administrator for this team; e.g. me@libre.space*

/label ~Service::ELN
/confidential
