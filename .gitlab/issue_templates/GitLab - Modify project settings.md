# Modify GitLab project settings #

## Project path ##

*Replace this line with the full project path; e.g. librespacefoundation/satnogs/satnogs-network*

## Settings ##

  - [ ] Private visibility

## Comments ##

*Replace this comment with any other information or setting that are not covered by the aforementioned placeholders or checklists*

/label ~Service::GitLab
/confidential
