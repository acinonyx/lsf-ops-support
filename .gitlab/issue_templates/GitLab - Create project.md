# Create GitLab project #

## Project name ##

*Replace this comment with the project name; e.g. SatNOGS Network*

## Project path ##

*Replace this comment with the full project path; e.g. librespacefoundation/satnogs/satnogs-network*

## Project types ##

  - [ ] Organizational
    - [ ] with issue templates
  - [ ] Web application
  - [ ] Docker image
  - [ ] Generic software
  - [ ] Mechanical CAD
  - [ ] Electronics CAD
  - [ ] Documentation
  - [ ] Service desk

## Project description (optional) ##

*Replace this comment with the project description; e.g. Global Management Network for SatNOGS*

## Project avatar ##

*Replace this comment with an attachment of a square-shaped PNG image with max file size of 200 KiB*

## Developers ##

  * *Replace this comment with a developer's GitLab username; e.g. @acinonyx*
  * *...*
  * *...*

## Settings ##

  - [ ] Private visibility

## Comments ##

*Replace this comment with any other information or setting that are not covered by the aforementioned placeholders or checklists*

/label ~Service::GitLab
/confidential
