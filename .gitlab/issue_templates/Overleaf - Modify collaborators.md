# Modify Overleaf project collaborators #

## Project name ##

*Replace this line with the project name*

## Add collaborators ##

  * *Replace this line with a collaborator's e-mail address*
  * *...*
  * *...*

## Remove collaborators ##

  * *Replace this line with a collaborator's e-mail address*
  * *...*
  * *...*

/label ~Service::Overleaf
/confidential
