# Modify Cloud user group #

## Group ##

*Replace this line with the group of users; e.g. Contributors*

## Add users ##

  * *Replace this line with a user's username*
  * *...*
  * *...*

## Remove users ##

  * *Replace this line with a user's username*
  * *...*
  * *...*

/label ~Service::Cloud
/confidential
