# Create email alias #

## Alias address ##

*Replace this line with the email address of the new alias; e.g. alias@libre.space*

## Recipient addresses ##

  * *Replace this line with a recipient's email address; e.g. recipient1@libre.space*
  * *...*
  * *...*

/label ~Service::Email
/confidential
