# Create Cloud user #

## Full name ##

*Replace this line with the user's full name*

## Username ##

*Replace this line with the user's username*

## Email address ##

*Replace this line with the user's e-mail address*

## Groups ##

  * *Replace this line with a user's group; e.g. Contributors*
  * *...*
  * *...*

/label ~Service::Cloud
/confidential
